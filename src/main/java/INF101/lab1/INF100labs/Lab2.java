package INF101.lab1.INF100labs;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int w1l = word1.length();
        int w2l = word2.length();
        int w3l = word3.length();

        int longest = Collections.max(Arrays.asList(w1l, w2l, w3l));
        if (w1l == longest) {
            System.out.println(word1);
        }
        if (w2l == longest) {
            System.out.println(word2);
        }
        if (w3l == longest) {
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                }
                else return false;
            }
            else return true;
        }
        else return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        return (num % 2 == 0 && (num > 0));
    }

}
