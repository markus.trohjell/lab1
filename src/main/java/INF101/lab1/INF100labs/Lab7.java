package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        //First check rows:
        int sumFirstRow = rowSum(grid, rowInd:0);
        for (int i = 1; i < grid.size(); i++) {
            int sumRow = rowSum(grid, i);
            if (sumFirstRow != sumRow) {
                return false;
            }
        }

        //Then check columns:
        int sumFirstCol = colSum(grid, colIndex: 0);
        for (int i = 1; i < grid.get(index:0).size(); i++) {
            int sumCol = colSum(grid, i);
            if (sumFirstCol != sumCol) {
                return false;
            }
        }
    }

    public static int rowSum(ArrayList<ArrayList<Integer>> grid, int rowInd) {
        int sum = 0;
        ArrayList<Integer> row = grid.get(rowInd);
        for (Integer x : row) {
            sum += x;
        }
        return  sum;
    }

    public static int colSum(ArrayList<ArrayList<Integer>> grid, int colIndex) {
        int sum = 0;
        for (int i = 0;< grid.get(index:0).size(); i++) {
            sum += grid.get(i).get(colIndex);
        }
        return sum;

    }

}