package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> List1 = new ArrayList<>();
        for (Integer x : list) {
            if (!(x==3)) {
                List1.add(x);
            }
        }
        return List1;

    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> List1 = new ArrayList<>();
        for (Integer x : list) {
            if (!List1.contains(x)) {
                List1.add(x);
            }
        }
        return List1;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i<a.size(); i++) {
            int aElem = a.get(i);
            int bElem = b.get(i);

            int sum = aElem+bElem;
            a.set(i, sum);
        }
    }

}