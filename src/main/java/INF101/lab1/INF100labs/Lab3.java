package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int x = 1; x<=n; x++) {
            if (x % 7 == 0) {
                System.out.println(x);
            }
        }

    }

    public static void multiplicationTable(int n) {
        for (int x = 1; x <= n; x++) {
            System.out.print(x+": ");
            for(int y = 1; y<=n; y++) {
                System.out.print(x*y + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        int crossSum = 0;
        while (num>0) {
            int rest = num % 10;
            num -= rest;
            crossSum+= rest;
            num = num/10;
        }
        return crossSum;
    }

}